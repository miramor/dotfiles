#!/bin/sh
if [ -x ~/.fehbg ]; then
	. ~/.fehbg
fi

# no system tray... yet!
compton -cCz &
plank &
